/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <libexec.h>

#include <stdio.h>
#include <stdlib.h>

#ifndef unlikely
#ifdef __GNUC__
#define unlikely(x) __builtin_expect (!!(x), 0)
#else
#define unlikely(x) (x)
#endif
#endif

int
libexec_addv (libexec l, const char *fmt, va_list ap)
{
  char *buf;
  // OutOfMemory is always unlikely in these times
  if (unlikely (vasprintf (&buf, fmt, ap) == -1))
    {
      return -1;
    }
  libexec_add (l, buf);
  free (buf);
  return 0;
}
