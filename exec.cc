/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libexec.h>
#include <libexec_priv.h>

#include <cerrno>
#include <cstdarg>
#include <cstdlib>
#include <cstring>
#include <glib.h>
#include <sys/wait.h>
#include <unistd.h>

extern "C" libexec_return
libexec_exec (libexec ex, ...) noexcept
{
  libexec_return ret = libexec_return_create ();
  if (ret == nullptr)
    return ret; // null indicates error
  va_list ap;
  va_start (ap, ex);
  if (ex->args.empty ())
    {
      ret->fail = true;
      va_end (ap);
      return ret;
    }
  bool do_exec = true;
  if (!(ex->flags & LIBEXEC_FLAG_NOFORK))
    {
      pid_t pid = fork ();
      if (pid == -1)
        {
          // Forking has failed
          ret->fail = true;
          va_end (ap);
          return ret;
        }
      if (pid == 0)
        {
          // The parent process
          ret->pid = pid;
          do_exec = false;
        }
      // Child
    }
  else
    {
      ret->pid = getpid ();
    }
  char *searchpath = (ex->flags & LIBEXEC_FLAG_PATH)
    ? g_strdup (va_arg (ap, const char *))
    : (ex->flags & LIBEXEC_FLAG_DEFPATH) ? libexec_defpath ()
    : g_strdup (getenv ("PATH"));
  char *execpath;
  if (ex->args[0].find (G_DIR_SEPARATOR_S) == std::string::npos)
    {
      // Find our executable on PATH
      execpath = libexec_pathsearch (ex->args[0].c_str (), searchpath);
      if (execpath == NULL)
        {
          errno = ENOENT;
          ret->failerrno = errno;
          ret->fail = true;
          return ret;
        }
    }
  else
    {
      execpath = g_strdup (ex->args[0].c_str ());
    }
  g_free (searchpath);
  if (do_exec)
    {
      char **args = (char **) std::malloc (sizeof (char *) * ex->args.size ());
      for (std::size_t i = 0; i < ex->args.size (); i++)
        {
          args[i] = (char *) std::malloc (ex->args[i].size () + 1);
          std::memcpy (args[i], ex->args[i].c_str (), ex->args[i].size () + 1);
        }
      // Clean up the most we can
      va_end (ap);

      execv (execpath, args);
      ret->fail = true;
      ret->failerrno = errno;
      return ret;
    }
  ret->execpath = execpath;
  g_free (execpath);
  if (ex->flags & LIBEXEC_FLAG_WAIT)
    {
      int status;
#ifndef WUNTRACED
#define WUNTRACED 0
#endif
      while (wait4 (ret->pid, &status, WUNTRACED, nullptr) != -1)
        {
#if defined (WIFEXITED) && defined (WEXITSTATUS)
          if (WIFEXITED (status))
            {
              ret->retstate = WEXITSTATUS (status);
              ret->exittype = 1; // Exited
              break;
            }
#endif
#if defined (WIFSIGNALED) && defined (WTERMSIG)
          if (WIFSIGNALED (status))
            {
              ret->retstate = WTERMSIG (status);
              ret->exittype = 2; // Signaled
              break;
            }
#endif
#if defined (WITSTOPPED) && defined (WSTOPSIG)
          if (WIFSTOPPED (status) && ex->flags & LIBEXEC_FLAG_WAITSTOP)
            {
              ret->retstate = WSTOPSIG (status);
              ret->exittype = 3; // Stopped
              break;
            }
#endif
        }
    }
  va_end (ap);
  return ret;
}
