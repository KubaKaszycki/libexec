/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LIBEXEC_H
#define _LIBEXEC_H

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

#ifdef __cplusplus
#if __cplusplus >= 201402L
#define __LIBEXEC_NOTHROW noexcept __attribute__ ((nothrow))
#else
#define __LIBEXEC_NOTHROW throw () __attribute__ ((nothrow))
#endif
#else
#define __LIBEXEC_NOTHROW __attribute__ ((nothrow))
#endif

struct __libexec;
struct __libexec_return;

typedef struct __libexec *libexec;
typedef struct __libexec_return *libexec_return;

#ifdef __cplusplus
extern "C" {
#endif

  // Life cycle management
  libexec libexec_create (void) __LIBEXEC_NOTHROW;
  void libexec_destroy (libexec) __LIBEXEC_NOTHROW;

  libexec_return libexec_return_create (void) __LIBEXEC_NOTHROW;
  void libexec_return_destroy (libexec_return) __LIBEXEC_NOTHROW;

/**
 * When executing program, do not fork. This means terminating present process.
 */
#define LIBEXEC_FLAG_NOFORK 0x1
/**
 * Use custom PATH for finding the executable. This makes next argument in
 * libexec_exec varargs of type const char *, containing colon- (Unix) or
 * semicolon- (Windows) separated directories.
 * 
 * @see libexec_pathsearch
 */
#define LIBEXEC_FLAG_PATH 0x2

/**
 * Waits for the child process to terminate.
 */
#define LIBEXEC_FLAG_WAIT 0x4

/**
 * If the child process is stopped (freezed), waiting is interrupted.
 */
#define LIBEXEC_FLAG_WAITSTOP 0x8

/**
 * Use the default path for system (the one obtained by confstr if present, or
 * a generic one).
 */
#define LIBEXEC_FLAG_DEFPATH 0x10

  libexec_return libexec_exec (libexec, ...) __LIBEXEC_NOTHROW;

  // Examining exec() return
  bool libexec_return_failed (libexec_return) __LIBEXEC_NOTHROW;
  int libexec_return_failed_errno (libexec_return) __LIBEXEC_NOTHROW;
  pid_t libexec_return_pid (libexec_return) __LIBEXEC_NOTHROW;
  int libexec_return_exitcode (libexec_return) __LIBEXEC_NOTHROW;

  // Adding
  int libexec_add (libexec, const char *) __LIBEXEC_NOTHROW;
  int libexec_append (libexec, const char *) __LIBEXEC_NOTHROW;
  int libexec_addf (libexec, const char *, ...) __LIBEXEC_NOTHROW;
  int libexec_appendf (libexec, const char *, ...) __LIBEXEC_NOTHROW;
  int libexec_addv (libexec, const char *, va_list) __LIBEXEC_NOTHROW;
  int libexec_appendv (libexec, const char *, va_list) __LIBEXEC_NOTHROW;

  // Misc operations
  size_t libexec_length (libexec) __LIBEXEC_NOTHROW;
  int libexec_remove (libexec, size_t) __LIBEXEC_NOTHROW;
  const char *libexec_get (libexec, size_t) __LIBEXEC_NOTHROW;

  // Path related utilities
  char *libexec_pathsearch (const char *, const char *) __LIBEXEC_NOTHROW;
  char *libexec_defpath (void) __LIBEXEC_NOTHROW;

  // Flags
  typedef uint32_t libexec_flags;

  libexec_flags libexec_flags_get (libexec) __LIBEXEC_NOTHROW;
  void libexec_flags_set (libexec, libexec_flags) __LIBEXEC_NOTHROW;
  libexec_flags libexec_flags_add (libexec, libexec_flags) __LIBEXEC_NOTHROW;
  libexec_flags libexec_flags_remove (libexec, libexec_flags) __LIBEXEC_NOTHROW;
  libexec_flags libexec_flags_only (libexec, libexec_flags) __LIBEXEC_NOTHROW;

#ifdef __cplusplus
}
#endif

#endif
