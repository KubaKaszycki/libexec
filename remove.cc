/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libexec.h>
#include <libexec_priv.h>

extern "C" int
libexec_remove (libexec l, size_t index) noexcept
{
  try
    {
      if (index >= l->args.size ())
        return -1;
      l->args.erase (l->args.begin () + index);
      return 0;
    }
  catch (...)
    {
      return -1;
    }
}
