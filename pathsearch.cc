/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libexec.h>

#include <cstring>
#include <glib.h>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

static inline bool
user_can_exe (const struct stat *sbuf)
{
  return (geteuid () == sbuf->st_uid) && (sbuf->st_mode & S_IRUSR) &&
    (sbuf->st_mode & S_IXUSR);
}

static inline bool
group_can_exe (const struct stat *sbuf)
{
  return (getegid () == sbuf->st_gid) && (sbuf->st_mode & S_IRGRP) &&
    (sbuf->st_mode & S_IXGRP);
}

static inline bool
other_can_exe (const struct stat *sbuf)
{
  return (sbuf->st_mode & S_IROTH) && (sbuf->st_mode & S_IXOTH);
}

extern "C" char *
libexec_pathsearch (const char *_prog, const char *_path) noexcept
{
  if (std::strstr (_prog, G_DIR_SEPARATOR_S) != nullptr)
    return g_strdup (_prog);
  struct stat sbuf;
  std::string prog{_prog}, path{_path};
  std::size_t index = 0;
  std::size_t old_index = 0;
  while (1)
    {
      old_index = index;
      index = path.find (G_SEARCHPATH_SEPARATOR_S, index + 1);
      if (old_index == std::string::npos)
        break;
      std::string filename{path.substr (old_index, index - old_index) + G_DIR_SEPARATOR_S
          + prog};
      if (stat (filename.c_str (), &sbuf) == -1)
        continue;
      if (user_can_exe (&sbuf) || group_can_exe (&sbuf)
          || other_can_exe (&sbuf))
        return g_strdup (filename.c_str ());
    }
  return NULL;
}
