/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libexec.h>
#include <libexec_priv.h>

extern "C" int
libexec_append (libexec l, const char *s) noexcept
{
  try 
    {
      if (l->args.size () > 0)
        l->args[l->args.size () - 1] += s;
      else
        l->args.push_back (s);
    }
  catch (...)
    {
      return -1;
    }
  return 0;
}
