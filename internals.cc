/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libexec.h>
#include <libexec_priv.h>

__libexec::__libexec () noexcept {}
__libexec::~__libexec () noexcept {}

__libexec_return::__libexec_return () noexcept {}
__libexec_return::~__libexec_return () noexcept {}

extern "C" libexec_flags
libexec_flags_get (libexec l) noexcept
{
  return l->flags;
}

extern "C" void
libexec_flags_set (libexec l, libexec_flags f) noexcept
{
  l->flags = f;
}

extern "C" libexec_flags
libexec_flags_add (libexec l, libexec_flags f) noexcept
{
  l->flags |= f;
  return l->flags;
}

extern "C" libexec_flags
libexec_flags_remove (libexec l, libexec_flags f) noexcept
{
  l->flags ^= f;
  return l->flags;
}

extern "C" libexec_flags
libexec_flags_only (libexec l, libexec_flags f) noexcept
{
  l->flags &= f;
  return l->flags;
}

extern "C" bool
libexec_return_failed (libexec_return r) noexcept
{
  return r->fail;
}

extern "C" int
libexec_return_exitcode (libexec_return r) noexcept
{
  return r->retstate;
}

extern "C" pid_t
libexec_return_pid (libexec_return r) noexcept
{
  return r->pid;
}

extern "C" int
libexec_return_failed_errno (libexec_return r) noexcept
{
  return r->failerrno;
}

extern "C" const char *
libexec_get (libexec l, std::size_t index) noexcept
{
  if (index >= l->args.size ())
    return nullptr;
  return l->args[index].c_str ();
}
