/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <libexec.h>

#include <glib.h>
#include <stdlib.h>
#if HAVE_UNISTD_H
# include <unistd.h>
#endif

char *
libexec_defpath (void)
{
#if HAVE_CONFSTR && defined (_CS_PATH)
  size_t confstr_sz;
  if ((confstr_sz = confstr (_CS_PATH, NULL, 0)) > 0)
    {
      char *buf = (char *) malloc (confstr_sz);
      confstr (_CS_PATH, buf, confstr_sz);
      return buf;
    }
#elif HAVE_PATHS_H && (defined (_PATH_DEFPATH) || defined (_PATH_STDPATH))
#ifdef _PATH_DEFPATH
#ifdef _PATH_STDPATH
  if (getuid () == 0)
    return g_strdup (_PATH_STDPATH);
  else
    return g_strdup (_PATH_DEFPATH);
#else
  return g_strdup (_PATH_DEFPATH);
#endif
#else
  return g_strdup (_PATH_STDPATH);
#endif
#else
  if (getuid () == 0)
    return g_strdup ("/usr/bin:/usr/sbin:/bin:/sbin");
  else
    return g_strdup ("/usr/bin:/bin");
#endif
}
