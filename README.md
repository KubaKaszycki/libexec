# libexec - EXECution LIBrary
**libexec** is a C library (with C++ backend) allowing to execute other
programs flexibly, customizably. It works currently on \*-nixes, but if you
install Cygwin, it should also work on Windows. It provides very simple
interface. See following example:

	#include <libexec.h>
	
	#include <stdio.h>
	
	int
	main (int argc, char **argv)
	{
		if (argc != 2)
			{
				fprintf (stderr, "Usage: compile <SOURCEFILE>\n");
				return 1;
			}
		libexec x = libexec_create ();
		libexec_add (x, "gcc");
		libexec_add (x, "-c");
		libexec_add (x, argv[1]);
		libexec_addf (x, "-frandom-seed=%s", argv[1]);
		libexec_exec (x);
		libexec_destroy (x);
		return 0;
	}

This example will execute the following command if given parameter `abc.c`:

	gcc -c abc.c -frand-seed=abc.c

The interface is very simple - `libexec_add` pushes next argument into the
list. `libexec_addf` uses printf-style format (internally, it uses `printf`
family function, `vasprintf`). There is also `libexec_addv`, which accepts a
`va_list`. The difference between them is the same as between `printf` and
`vprintf`.

There is also a function called `libexec_append`. It has parameter types,
return type and variants same as `libexec_add`. It differs in fact, that if the
execution list is not empty, it appends to the last item rather than creating a
new one. But don't use it just for adding first element, it's slower.

Even though the internals of many functions are written in C++, externally
there are no C++ specific features to be are cared about. When writing in C++,
you may also include the header `<libexec_priv.h>` which is in C++ and allows
access to some of libexec's internals.

# Flags
`libexec_exec` accepts **flags**. These are simple variables, affecting the
control of execution. When mentioning the "next vararg", it means the next
variable argument. Variable arguments of flags are read in the same order as
the order here.

## `LIBEXEC_FLAG_NOFORK`
If set, child is not `fork()`ed (executed in separate process), but run
directly, as in `execvp()` call, as opposed to `system()`. This prevents all
flags related to pipes or waiting.

## `LIBEXEC_FLAG_PATH`
If set, the next vararg shall be of type `const char *`, this will be the
search path for program. Note that even if the search path is set, if the
program name contains a slash, the path is unused. This shall be separated
with the same delimiter as you would set in `$PATH`, colon (`:`) on most
systems and semicolon (`;`) on Windows.

## `LIBEXEC_FLAG_WAIT`
If set, after executing the program in a separate child process, the parent
process will wait until execution of child terminates, either by killing it
(but **not** stopping it), or by a clean exit. This also fills in the exit
code, or the killing signal for a kill.

## `LIBEXEC_FLAG_WAITSTOP`
This flag is unused if `LIBEXEC_FLAG_WAIT` is unset. If set, it can also wait
for the process being stopped and reacts to it the same way as it would react
to a kill.

## `LIBEXEC_FLAG_DEFPATH`
This flag is the same as `LIBEXEC_FLAG_PATH`, except that it does not accept an
argument. Instead, it takes the default path for system, determined using
`confstr(_CS_PATH)`, `_PATH_DEFPATH` or `_PATH_STDPATH`. If none is present, a
generic path is hard-coded into the program, `/usr/bin:/bin` for normal user
and `/usr/bin:/usr/sbin:/bin:/sbin` for root (UID 0).
