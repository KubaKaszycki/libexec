/*
 * Copyright (C) 2016  Jakub Kaszycki
 * This file is a part of libexec.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LIBEXEC_PRIV_H
#define _LIBEXEC_PRIV_H

#include <libexec.h>

#include <string>
#include <sys/types.h>
#include <vector>

class __libexec
{
public:
  __libexec (void) __LIBEXEC_NOTHROW;
  virtual ~__libexec (void) __LIBEXEC_NOTHROW;

  std::vector<std::string> args;
  libexec_flags flags = 0;
};

class __libexec_return
{
public:
  __libexec_return (void) __LIBEXEC_NOTHROW;
  virtual ~__libexec_return (void) __LIBEXEC_NOTHROW;

  pid_t pid;
  int retstate = 0;
  bool fail = false;
  int failerrno = 0;
  bool signaled = false;
  /**
   * Exit type. Possible values are:
   * <table>
   * <tr><th>Value</th><th>Exit type</th></tr>
   * <tr><td>0</td><td>Did not exit (still running)</td></tr>
   * <tr><td>1</td><td>Exited normally (retstate is exit code)</td></tr>
   * <tr><td>2</td><td>Killed (retstate is killing signal)</td></tr>
   * <tr><td>3</td><td>Stopped (retstate is stopping signal)</td></tr>
   * </table>
   */
  int exittype = 0;
  std::string execpath;
};

#endif
